/******************************************************************************
Run using GCC C Compiler
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/* Compute Slope */
// Input: y = coordinate values and
//      : n = number of inputs
// Output: slope = slope of the online
//       : b = offset
//Calculations : 
//              slope = ((xmean*ymean) - xymean)/(xmeansqr - xsqrmean)
//              b = ymean - slope*xmean;
float * slopenoffset(int *y, int n){
    float *output;
    int x;
    float slope, b, xmean=0.0, ymean=0.0, xymean=0.0, xmeansqr=0.0, xsqrmean=0.0;
    output = (float *)malloc(2*sizeof(float));
    for(x=0; x<n; x++){
        xmean = xmean + x;
        ymean = ymean + *(y+x);
        xymean = xymean + x*(*(y+x));
        xsqrmean = xsqrmean + x*x;    
    }
    xmean = xmean/n;
    ymean = ymean/n;
    xymean = xymean/n;
    xmeansqr = xmean*xmean;
    xsqrmean = xsqrmean/n;
    slope = ((xmean*ymean) - xymean)/(xmeansqr - xsqrmean);
    b = ymean - slope*xmean;
    *(output+0) = slope;
    *(output+1) = b;
    return output;
}
/* Check whether element is present in the array */
// Input: y = coordinate values and
//      : x = computed x value
//      : key = search value
//      : n = number of elements
// Output: index position of the element
int isfound(int *y, int x, int key, int n){
    if (*(y+x) == key){
        return x;        
    }
    else if(x > 0 && x < n){
        if(*(y+x-1) == key){
            return x-1;
        }
        else if (*(y+x) == key){
            return x+1;
        }    
    }
    return -1;
}
int main(){
    int *y, n, i, key, ind;
    float *mb, m, b, x;
    printf("Enter number of elements:\n");
    scanf("%d", &n);
    y = (int *) malloc(n*sizeof(int));
    printf("Enter the integer values:\n");
    //Get the y coordinate values
    for (i=0;i<n;i++){
        scanf("%d", y+i);
    }
    mb = slopenoffset(y, n);
    m = *(mb + 0);
    b = *(mb + 1);
    printf("Enter the key to be searched:\n");
    scanf("%d", &key);
    x=(key-b)/m;    
    ind = isfound(y, (int)round(x), key, n);
    printf("The key is found at :%d", ind);
    return 0;
}